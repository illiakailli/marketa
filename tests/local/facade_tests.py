from marketa import get_facade
from marketa.database.repository import Repository
from marketa.facade import Facade

from ..helpers.container import Container
from ..helpers.data_helper import DataHelper


def test_can_get_facade():
    mk = get_facade()
    assert mk is not None


def test_can_get_watchlist_symbols():
    container = Container()
    helper = container.resolve(DataHelper)
    helper.reseed_test_data()
    mk = container.resolve(Facade)
    symbols = mk.get_watchlist_symbols()

    assert len(symbols) == 1


def test_can_add_watchlist_item():
    # setup
    container = Container()
    repo = container.resolve(Repository)
    helper = container.resolve(DataHelper)
    helper.reseed_test_data()
    syms_before = repo.get_watchlist_symbols()
    mk = container.resolve(Facade)

    # act
    mk.watch("SPY")

    # verify
    syms_after = repo.get_watchlist_symbols()
    assert len(syms_after) == len(syms_before) + 1


def test_can_remove_watchlist_item():
    # setup
    container = Container()
    repo = container.resolve(Repository)
    helper = container.resolve(DataHelper)
    helper.reseed_test_data()
    syms_before = repo.get_watchlist_symbols()
    mk = get_facade()

    # act
    mk.unwatch("AAAU")

    # verify
    syms_after = repo.get_watchlist_symbols()
    assert len(syms_after) == len(syms_before) - 1


def test_purge_doesnt_throw():
    mk = get_facade()
    mk.purge()


def test_can_get_tradable_symbols():
    container = Container()
    helper = container.resolve(DataHelper)
    helper.reseed_test_data()
    mk = get_facade()

    symbols = mk.get_tradable_symbols()

    assert len(symbols) == 3
