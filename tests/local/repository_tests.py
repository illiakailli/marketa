from marketa.database.repository import Repository

from ..helpers.container import Container
from ..helpers.data_helper import DataHelper
from ..helpers.repository_in_memory import RepositoryInMemory


def test_ensure_singleton_lifetime():
    container = Container()
    repo1 = container.resolve(Repository)
    repo2 = container.resolve(Repository)
    assert repo1 is repo2


def test_can_create_database_in_memory():
    # setup: override default repository with in-memory one
    container = Container()
    container.register_singleton_pair(Repository, RepositoryInMemory, override=True)
    repo = container.resolve(Repository)
    helper = container.resolve(DataHelper)

    # act
    helper.reseed_test_data()

    # verify
    assert type(repo) is RepositoryInMemory
    markets = repo.get_markets()
    assert len(markets) == 2


def test_can_create_database():
    # setup
    container = Container()
    repo = container.resolve(Repository)
    helper = container.resolve(DataHelper)

    # act
    helper.reseed_test_data()

    # verify
    markets = repo.get_markets()
    assert len(markets) == 2

    instruments = repo.get_instruments()
    assert len(instruments) == 3

    market = repo.get_market_by_name("TSTMKT1")
    prices = repo.get_prices_intraday(market.id, "SYM1")
    assert len(prices) == 3
