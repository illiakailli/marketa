from marketa.feeds.nasdaq import FeedNasdaq


def test_can_load_nasdaq_instruments():
    feed = FeedNasdaq()
    result = feed.load_instruments()

    assert result is not None
    assert len(result) > 0
    for item in result:
        assert item.name is not None
        assert item.symbol is not None


def test_load_nasdaq_instrument_data():
    feed = FeedNasdaq()
    data = feed.load_instrument_data("spy")

    assert data.instrument.name is not None
    assert len(data.prices_daily) > 0


def test_can_load_nasdaq_market_data():
    feed = FeedNasdaq()
    data = feed.load_market_data()

    assert data.name is not None
