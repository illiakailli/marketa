from pandas import DataFrame

from marketa.facade import Facade

from ..helpers.container import Container
from ..helpers.data_helper import DataHelper


def test_can_pull_watchlist_symbols_data():
    # setup
    container = Container()
    facade = container.resolve(Facade)
    helper = container.resolve(DataHelper)
    helper.reseed_test_data()

    # act
    facade.pull()

    # verify
    prices = facade.get_prices("AAAU")
    assert type(prices) is DataFrame
    assert not prices.empty
