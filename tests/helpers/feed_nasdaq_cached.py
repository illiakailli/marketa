from typing import Iterable

from diskcache import Cache

from marketa.database.instrument import Instrument
from marketa.database.instrument_feed_data import InstrumentFeedData
from marketa.database.market import Market
from marketa.feeds.nasdaq import FeedNasdaq
from marketa.shared.utils import check_internet, get_cache_folder_path

from ..exceptions import MarketaTestException


class FeedNasdaqCached:
    def __init__(self):
        self._feed_nasdaq = FeedNasdaq()

    def get_market_name(self):
        return self._feed_nasdaq.get_market_name()

    def load_market_data(self) -> Market:
        result = Market()
        result.name = self._feed_nasdaq.get_market_name()
        result.description = 'Nasdaq is a global electronic marketplace for buying and selling securities. Originally an acronym for "National Association of Securities Dealers Automated Quotations"—it was a subsidiary of the National Association of Securities Dealers (NASD), now known as the Financial Industry Regulatory Authority (FINRA). Nasdaq was created as a site where investors could trade securities on a computerized, speedy, and transparent system.1 It commenced operations on Feb. 8, 1971'
        return result

    def load_instruments(self) -> Iterable[Instrument]:
        p = get_cache_folder_path()
        with Cache(directory=p) as cache:
            pkey = "nasdaq_listed_symbols_cache_permanent"
            data = cache.get(pkey)
            if data is not None:
                # there is some data in cache, so just return first 20, should be enough for tests
                first20 = list(map(lambda x: data[x], range(20)))
                return first20

            online = check_internet()
            if not online:
                msg = "unable to load nasdaq listed symbols as there is no internet connection available and no cached data was found"
                raise MarketaTestException(msg)

            # we're online and cache is empty, so fetch fresh data for traded and index symbols
            data = self._feed_nasdaq.load_instruments()
            cache.set(pkey, data)  # update cache

            first20 = list(map(lambda x: data[x], range(20)))
            return first20

    def load_instrument_data(self, symbol: str) -> InstrumentFeedData:
        p = get_cache_folder_path()
        with Cache(directory=p) as cache:
            pkey = f"yfinance_ticker_{symbol}_cache_permanent"
            data = cache.get(pkey)
            if data is not None:
                return data  # there is some data in cache, so just return it

            online = check_internet()
            if not online:
                # we're offline and there is no data in cache
                msg = f'unable to load yfinance "{symbol}" symbol  data as there is no internet connection available and no cached data was found'
                raise MarketaTestException(msg)

            # we're online and cache is empty, so fetch fresh data for symbol
            data = self._feed_nasdaq.load_instrument_data(symbol)
            cache.set(pkey, data)  # update cache

            return data
