import random
import string
from datetime import datetime, timedelta

from marketa.database.instrument import Instrument
from marketa.database.instrument_feed_data import InstrumentFeedData
from marketa.database.market import Market
from marketa.database.price_intraday import PriceIntraday
from marketa.database.repository import Repository
from marketa.database.security_type import SecurityType

from .. import constants


class DataHelper:
    def __init__(self, repository: Repository):
        self._repository = repository

    def reseed_test_data(self):
        self._repository.recreate_all()
        m1 = self.create_test_market("TSTMKT1")
        m2 = self.create_test_market("TSTMKT2")

        i1 = self.create_test_instrument("SYM1")
        i2 = self.create_test_instrument("SYM2")
        i3 = self.create_test_instrument("SYM3")

        self._repository.add_instrument_to_market(m1.id, i1.id)
        self._repository.add_instrument_to_market(m1.id, i2.id)
        self._repository.add_instrument_to_market(m2.id, i3.id)

        self.create_test_intraday_prices(m1.id, i1.id)

    def create_test_market(self, name: str):
        data = Market()
        data.name = name
        return self._repository.create_market(data)

    def create_test_instrument(self, symbol=constants.test_symbol) -> Instrument:
        data = Instrument()
        if symbol:
            data.symbol = symbol
        else:
            data.symbol = f"TST{random_symbol_name_generator(4)}"
        data.name = f"test symbol {data.symbol} name"
        data.type = SecurityType.stock
        data.description = f"test stock {symbol} description"
        data.currency = "CAD"
        return self._repository.create_instrument(data)

    def create_test_intraday_prices(self, market_id, instrument_id):
        # create test prices
        prices = []
        price = PriceIntraday()
        price.price = 5.55
        price.datetime = datetime.now() - timedelta(hours=2)
        prices.append(price)
        price = PriceIntraday()
        price.price = 6.66
        price.datetime = datetime.now() - timedelta(hours=1)
        prices.append(price)
        price = PriceIntraday()
        price.price = 9.03
        price.datetime = datetime.now()
        prices.append(price)

        data = InstrumentFeedData()
        data.market_id = market_id
        data.instrument_id = instrument_id
        data.prices_intraday = prices
        self._repository.upsert_prices_intraday(data)


def random_symbol_name_generator(str_size, allowed_chars=string.ascii_uppercase):
    return "".join(random.choice(allowed_chars) for x in range(str_size))
