from marketa.feeds.nasdaq import FeedNasdaq
from marketa.shared.container import Container as RealContainer
from tests.helpers.feed_nasdaq_cached import FeedNasdaqCached


class Container(RealContainer):
    """container used by tests that swaps real feeds service for a faked (cached) one"""

    def __init__(self):
        super().__init__()
        self.register_singleton_pair(FeedNasdaq, FeedNasdaqCached, override=True)
