from sqlalchemy import create_engine

from marketa.database.repository import Repository


class RepositoryInMemory(Repository):
    def __init__(self):
        super().__init__(in_memory=True)

    def _create_engine(self):
        self._engine = create_engine("sqlite://", echo=False)
        self._engine.execute("ATTACH DATABASE ':memory:' AS my_database")
