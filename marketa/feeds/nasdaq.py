import io
from decimal import Decimal
from typing import Final, Iterable, List

import pandas as pd

# trunk-ignore(mypy/import)
# trunk-ignore(mypy/note)
import requests
import yfinance as yf
from pandas import DataFrame
from purl import URL

# trunk-ignore(flake8/F401)
import marketa.shared.extensions  # used by some methods in this module
from marketa.feeds.dtos import Market, MarketState
from marketa.shared.constants import marketname_nasdaq

from ..database.instrument import Instrument
from ..database.instrument_feed_data import InstrumentFeedData
from ..database.price_daily import PriceDaily
from ..database.price_intraday import PriceIntraday
from ..database.security_type import SecurityType
from ..shared.exceptions import MarketaException


class FeedNasdaq:
    """Responsible for importing data from nasdaq.com website pages that contain securities feeds.
    Feeds are produced by online stock screener. Stock screeners are tools that allow investors
    and traders to sort through thousands of individual securities to find those that fit their own needs."""

    headers: Final = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36"
    }

    def __init__(self):
        pass

    def get_market_name(self):
        return marketname_nasdaq

    def load_market_data(self) -> Market:
        # load any ticker to get exchange data
        url = URL("https://query2.finance.yahoo.com/v7/finance/quote?symbols=AAAU")
        json = requests.get(url, headers=self.headers).json()
        data = json["quoteResponse"]["result"][0]

        result = Market()
        result.name = marketname_nasdaq
        result.state = MarketState.parse(data["marketState"])
        result.timezone_name = data["exchangeTimezoneName"]

        return result

    def load_instruments(self) -> Iterable[Instrument]:
        data = DataFrame()
        data_traded = self.__fetch_nasdaq_traded_symbols()
        data_index = self.__fetch_nasdaq_index_symbols()

        # merge results into a single dataframe
        data = data.append(data_traded)
        data = data.append(data_index)
        data.reset_index(
            drop=True,  # removes old incorrect integer row index and replaces it with updated one
            inplace=True,
        )

        # convert into a typed collection
        result: List[Instrument] = []
        for idx, row in data.iterrows():
            instrument = Instrument()
            instrument.symbol = row["symbol"]
            instrument.name = row["name"]
            instrument.type = SecurityType.parse(row["type"])
            result.append(instrument)
        return result

    def load_instrument_data(
        self, symbol: str, load_history=True
    ) -> InstrumentFeedData:
        result = InstrumentFeedData()
        result.market = self.load_market_data()
        instrument = Instrument()
        result.instrument = instrument

        price_intraday = PriceIntraday()
        price_daily_today = PriceDaily()

        t = yf.Ticker(symbol)

        instrument.symbol = symbol
        instrument.name = t.info["longName"]

        if "quoteType" in dict(t.info).keys():
            instrument.type = SecurityType.parse(t.info["quoteType"])

        if "longBusinessSummary" in dict(t.info).keys():
            instrument.description = t.info["longBusinessSummary"]

        if "currency" in dict(t.info).keys():
            instrument.currency = t.info["currency"]

        if (
            "regularMarketVolume" in dict(t.info).keys()
            and t.info["regularMarketVolume"] is not None
        ):
            price_daily_today.volume = int(t.info["regularMarketVolume"])

        price_daily_today.open = Decimal(
            str(t.info["open"])
        )  # str to avoid precision issues
        price_daily_today.high = t.info["dayHigh"]
        price_daily_today.low = t.info["dayLow"]
        # close is ambiguous here, as it seems we don't have field indicating if market closed or not
        # and regularMarketPreviousClose may indicate close for previous day
        # if not t.info['tradeable']:
        #    price_daily_today.last_close = self.price # after market closes, last_close should equal to price
        # else:
        #    self.last_close = t.info['regularMarketPreviousClose']

        price_intraday.price = Decimal(
            str(t.info["regularMarketPrice"])
        )  # str to avoid precision issues
        price_intraday.bid = t.info["bid"]
        price_intraday.bid_size = t.info["bidSize"]
        price_intraday.ask = t.info["ask"]
        price_intraday.ask_size = t.info["askSize"]
        result.prices_intraday = [price_intraday]

        result.prices_daily = []
        if load_history:
            history: DataFrame = yf.download(symbol, period="max", progress=False)
            if len(history.index) == 0:
                raise Exception(f'no historical data for "{symbol}" symbol')

            for idx, row in history.iterrows():
                price_item = PriceDaily()
                price_item.open = row["Open"]
                price_item.close = row["Close"]
                price_item.high = row["High"]
                price_item.low = row["Low"]
                price_item.volume = row["Volume"]
                price_item.date = row.name.to_pydatetime().date()
                result.prices_daily.insert(0, price_item)

        # we will provide potentially incomplete aggregate values available
        result.prices_daily.append(price_daily_today)

        return result

    def __fetch_nasdaq_traded_symbols(self) -> DataFrame:
        url = URL("https://www.nasdaqtrader.com/dynamic/SymDir/nasdaqtraded.txt")
        csv_text = requests.get(url, headers=self.headers).text
        df: DataFrame = pd.read_csv(io.StringIO(csv_text), sep="|")

        # drop test rows
        # todo: reimplement using adapter pattern for DataFrame
        df.drop_rows(column="Test Issue", predicate=lambda c: c == "Y")

        df.drop(
            columns=[
                "Nasdaq Traded",
                "Test Issue",
                "Round Lot Size",
                "Market Category",
                "Listing Exchange",
                "Financial Status",
                "CQS Symbol",
                "NASDAQ Symbol",
                "NextShares",
            ],
            inplace=True,
        )

        df.rename(
            columns={
                "Symbol": "symbol",
                "Security Name": "name",
                "ETF": "etf",
            },
            inplace=True,
        )

        df["type"] = df["etf"].apply(
            lambda x: SecurityType.stock.name if x == "N" else SecurityType.etf.name
        )
        df.drop(columns=["etf"], inplace=True)

        return df

    def __fetch_nasdaq_index_symbols(self) -> DataFrame:
        df = DataFrame()
        url = URL("https://api.nasdaq.com/api/screener/index")

        # we can only query 50 pages per request, otherwise it won't work
        offset = 0
        while True:
            url = URL("https://api.nasdaq.com/api/screener/index?offset=" + str(offset))
            json = requests.get(url, headers=self.headers).json()
            rows = json["data"]["records"]["data"]["rows"]
            for irow, row in enumerate(rows):
                df.append(
                    pd.Series(dtype="object"), ignore_index=True
                )  # insert empty row
                for col_name in row:
                    df.loc[irow + offset, col_name] = row[col_name]
            offset += 50
            if offset > json["data"]["records"]["totalrecords"]:
                break

        df.drop(
            columns=[
                "lastSalePrice",
                "netChange",
                "percentageChange",
                "deltaIndicator",
            ],
            inplace=True,
        )
        df.rename(columns={"companyName": "name"}, inplace=True)
        df = df.assign(
            type=SecurityType.index.name
        )  # adds 'type' attribute with 'index' value to all rows
        if "symbol" not in df.columns:
            raise MarketaException('nasdaq index feed is missing "symbol" column')
        return df
