from datetime import date, datetime
from decimal import Decimal
from enum import Enum
from typing import Iterable

from ..shared.exceptions import MarketaException


class Market:
    def __init__(self):
        self.name: str = None
        self.state: MarketState = None
        self.timezone_name: str = None


class MarketState(Enum):
    prepre = 0
    pre = 1
    regular = 2
    post = 3
    postpost = 4
    closed = 5

    @staticmethod
    def parse(s: str):
        if s.upper() == "PREPRE":
            return MarketState.prepre
        if s.upper() == "PRE":
            return MarketState.pre
        if s.upper() == "REGULAR":
            return MarketState.regular
        if s.upper() == "POST":
            return MarketState.post
        if s.upper() == "POSTPOST":
            return MarketState.postpost
        if s.upper() == "CLOSED":
            return MarketState.closed
        raise MarketaException(f"unknown market state {s}")


class SecurityType(Enum):
    stock = 0  # common stock
    mutual = 1  # mutual fund
    etf = 2  # exchange traded fund
    index = 3  # index fund

    @staticmethod
    def parse(s: str):
        if s.upper() == "ETF":
            return SecurityType.etf
        if s.upper() == "INDEX":
            return SecurityType.index
        if s.upper() == "STOCK":
            return SecurityType.stock
        if s.upper() == "MUTUALFUND":
            return SecurityType.mutual
        raise MarketaException(f"unknown security type {s}")


class PriceIntraday:
    def __init__(self):
        self.market: str = None
        self.symbol: str = None

        # orderbook aggregate data
        self.bid: Decimal = None
        self.bid_size: int = None
        self.ask: Decimal = None
        self.ask_size: int = None

        # price
        self.price: Decimal = None
        self.datetime: datetime = None


class PriceDaily:
    def __init__(self):
        self.market: str = None
        self.symbol: str = None

        self.open: Decimal = None
        self.close: Decimal = None
        self.high: Decimal = None
        self.low: Decimal = None
        self.volume: int = None
        self.date: date = None


class Instrument:
    def __init__(self):
        self.name: str = None
        self.symbol: str = None
        self.type: SecurityType = None
        self.description: str = None
        self.currency: str = None


class PriceHistory:
    def __init__(self):
        self.market: str = None
        self.symbol: str = None
        self.prices_intraday: Iterable[PriceIntraday] = []
        self.prices_daily: Iterable[PriceDaily] = []
