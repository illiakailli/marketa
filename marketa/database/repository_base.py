from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from marketa.database.session_wrapper import SessionWrapper

from .base import Base


class RepositoryBase:
    def __init__(self, in_memory=False):
        self._seeded = False
        self._seeding = False
        self._create_engine()

    def get_path(self):
        return Path(__file__).parent.parent.joinpath("database.sqlite").resolve()

    def recreate_all(self):
        Base.metadata.drop_all(self._engine)
        self._seeded = False
        self._ensure_seeded()

    def _create_engine(self):
        path = self.get_path()
        self._engine = create_engine(f"sqlite:///{path}", echo=False)

    def _session_begin(self, parent: Session = None) -> Session:
        if not parent:
            if not self._seeding:  # check to avoid recursion
                self._ensure_seeded()
            return sessionmaker(bind=self._engine, expire_on_commit=False).begin()
        else:
            return SessionWrapper(parent)

    def _ensure_seeded(self):
        if self._seeded:
            return
        self._seeding = True
        Base.metadata.create_all(self._engine)
        self._seeding = False
        self._seeded = True
