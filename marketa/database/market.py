from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from marketa.database.market_instrument import MarketInstrument

from .base import Base


class Market(Base):
    """Market where financial instruments are traded"""

    __tablename__ = "market"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    timezone_name = Column(String)

    instruments = relationship("Instrument", secondary=MarketInstrument.__tablename__)
