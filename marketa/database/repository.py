from typing import Iterable

from sqlalchemy.orm import Session, load_only

from marketa.database.instrument_feed_data import InstrumentFeedData
from marketa.feeds.dtos import Market as MarketData
from marketa.shared.exceptions import MarketaException

from ..shared.utils import gmt_now
from .instrument import Instrument
from .market import Market
from .price_daily import PriceDaily
from .price_intraday import PriceIntraday
from .repository_base import RepositoryBase
from .watchlist import Watchlist


class Repository(RepositoryBase):
    def __init__(self, in_memory=False):
        super().__init__(in_memory)

    def upsert_market(self, data: MarketData) -> Market:
        with self._session_begin() as session:
            market = (
                session.query(Market).filter(Market.name == data.name).one_or_none()
            )
            if not market:
                market = Market()
                session.add(market)

            market.name = data.name
            market.timezone_name = data.timezone_name
            return market

    def create_market(self, market: Market):
        with self._session_begin() as session:
            session.add(market)
            return market

    def get_markets(self) -> Iterable[Market]:
        with self._session_begin() as session:
            return session.query(Market).all()

    def get_market_by_name(self, name) -> Market:
        with self._session_begin() as session:
            market = session.query(Market).filter(Market.name == name).one()
            return market

    def create_instrument(self, data: Instrument, parent: Session = None) -> Instrument:
        with self._session_begin(parent) as session:
            if data.symbol is None:
                raise MarketaException("symbol for the instrument is none")
            instrument = Instrument()
            instrument.name = data.name
            instrument.symbol = data.symbol
            instrument.type = data.type
            instrument.description = data.description
            instrument.currency = data.currency
            session.add(instrument)
            return instrument

    def add_instrument_to_market(self, market_id, instrument_id):
        with self._session_begin() as session:
            market = session.query(Market).filter(Market.id == market_id).one()
            instrument = (
                session.query(Instrument).filter(Instrument.id == instrument_id).one()
            )
            market.instruments.append(instrument)

    def get_instruments(self) -> Iterable[Instrument]:
        with self._session_begin() as session:
            return session.query(Instrument).all()

    def get_unique_symbols(self) -> Iterable[str]:
        with self._session_begin() as session:
            items = (
                session.query(Instrument)
                .options(load_only(Instrument.symbol))
                .distinct()
                .all()
            )
            return list(map(lambda x: x.symbol, items))

    def get_watchlist_symbols(self) -> Iterable[str]:
        with self._session_begin() as session:
            items = session.query(Watchlist).all()
            if len(items) == 0:
                return []
            return list(map(lambda x: x.symbol, items))

    def add_watchlist_item(self, symbol: str):
        with self._session_begin() as session:
            item = Watchlist()
            item.symbol = symbol
            session.add(item)

    def remove_watchlist_item(self, symbol: str):
        with self._session_begin() as session:
            watchlist: Watchlist = (
                session.query(Watchlist).filter(Watchlist.symbol == symbol).one()
            )
            session.delete(watchlist)

    def get_prices_intraday(self, market_id, symbol: str) -> Iterable[PriceIntraday]:
        with self._session_begin() as session:
            market: Market = session.query(Market).filter(Market.id == market_id).one()
            instrument: Instrument = (
                session.query(Instrument).filter(Instrument.symbol == symbol).one()
            )
            prices: PriceIntraday = (
                session.query(PriceIntraday)
                .filter(
                    PriceIntraday.instrument_id == instrument.id,
                    PriceIntraday.market_id == market.id,
                )
                .all()
            )
            return prices

    def get_prices_daily(self, market_id, symbol: str) -> Iterable[PriceDaily]:
        with self._session_begin() as session:
            market: Market = session.query(Market).filter(Market.id == market_id).one()
            instrument: Instrument = (
                session.query(Instrument).filter(Instrument.symbol == symbol).one()
            )
            prices: PriceDaily = (
                session.query(PriceDaily)
                .filter(
                    PriceDaily.instrument_id == instrument.id,
                    PriceDaily.market_id == market.id,
                )
                .all()
            )
        return prices

    def upsert_instruments(self, instruments):
        with self._session_begin() as session:
            for instrument in instruments:
                self.upsert_instrument(instrument, session)

    def upsert_instrument(self, data: Instrument, parent: Session = None) -> Instrument:
        with self._session_begin(parent) as session:
            instrument: Instrument = (
                session.query(Instrument)
                .filter(Instrument.symbol == data.symbol)
                .one_or_none()
            )
            if instrument is None:
                instrument = self.create_instrument(data, session)
            else:
                if data.symbol is not None:
                    instrument.symbol = data.symbol
                if data.name is not None:
                    instrument.name = data.name
                if data.description is not None:
                    instrument.description = data.description
                if data.type is not None:
                    instrument.type = data.type
                if data.currency is not None:
                    instrument.currency = data.currency
            return instrument

    def upsert_instrument_data(self, data: InstrumentFeedData):
        with self._session_begin() as session:
            instrument = self.upsert_instrument(data.instrument, session)
            data.instrument_id = instrument.id
            self.upsert_prices_intraday(data)
            self.upsert_prices_daily(data)

    def upsert_prices_intraday(self, data: InstrumentFeedData):
        with self._session_begin() as session:
            for price in data.prices_intraday:
                now = gmt_now()
                price.datetime = now
                price.imported_at = now
                price.instrument_id = data.instrument_id
                price.market_id = data.market_id

                market: Market = (
                    session.query(Market).filter(Market.id == data.market_id).one()
                )
                instrument: Instrument = (
                    session.query(Instrument)
                    .filter(Instrument.id == data.instrument_id)
                    .one()
                )
                price.instrument_id = instrument.id
                price.market_id = market.id
                session.add(price)

    """
    def insert_price_intraday(self, data: PriceIntradayData):
        with self._session_begin() as session:
            market: Market = (
                session.query(Market).filter(Market.id == data.market_id).one()
            )
            entity = PriceIntraday()
            
            entity.price = data.price
            entity.market_id = market.id
            session.add(entity)
    """

    def upsert_prices_daily(self, data: InstrumentFeedData, parent: Session = None):
        with self._session_begin(parent) as session:
            now = gmt_now()
            for price_daily in data.prices_daily:
                price_daily.market_id = data.market_id
                price_daily.instrument_id = data.instrument_id
                price_daily.imported_at = now
                session.add(price_daily)

    def _ensure_seeded(self):
        if self._seeded:
            return
        super(Repository, self)._ensure_seeded()
        self._seeding = True

        # seed database if there are no symbols yet
        if len(self.get_watchlist_symbols()) == 0:
            self.add_watchlist_item("AAAU")
            # self.add_watchlist_item('SPX')

        self._seeding = False
        self._seeded = True
