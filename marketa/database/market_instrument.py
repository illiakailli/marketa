from sqlalchemy import Column, ForeignKey, Integer

from .base import Base


class MarketInstrument(Base):
    __tablename__ = "market_instruments"
    id = Column(Integer, primary_key=True)
    market_id = Column(Integer, ForeignKey("market.id"))
    instrument_id = Column(Integer, ForeignKey("instrument.id"))
