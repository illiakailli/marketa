from sqlalchemy.orm import Session


class SessionWrapper:
    def __init__(self, session: Session):
        self.session = session

    def __enter__(self):
        return self.session

    def __exit__(self, type, value, traceback):
        pass  # we're intentionally not closing wrapped session
