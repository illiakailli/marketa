from sqlalchemy import Column, Enum, Integer, String
from sqlalchemy.orm import relationship

from marketa.database.market import MarketInstrument

from .base import Base
from .security_type import SecurityType


class Instrument(Base):
    """Financial instruments are assets that can be traded, or they
    can also be seen as packages of capital that may be traded."""

    __tablename__ = "instrument"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    symbol = Column(String)
    type = Column(Enum(SecurityType))
    description = Column(String)
    currency = Column(String)

    markets = relationship("Market", secondary=MarketInstrument.__tablename__)
