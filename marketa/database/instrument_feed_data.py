from typing import Iterable

from marketa.database.instrument import Instrument
from marketa.database.price_daily import PriceDaily
from marketa.database.price_intraday import PriceIntraday


class InstrumentFeedData:
    def __init__(self):
        self.instrument: Instrument = None
        self.prices_intraday: Iterable[PriceIntraday] = None
        self.prices_daily: Iterable[PriceDaily] = None

        # not provided by feed service, but simplifies some method signatures later
        self.market_id = None
        self.instrument_id = None
