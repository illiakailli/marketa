from sqlalchemy import Column, Integer, String

from .base import Base


class Watchlist(Base):
    """Watchlist is a set of securities that an investor monitors
    for potential trading or investing opportunities"""

    __tablename__ = "watchlist"

    id = Column(Integer, primary_key=True)
    symbol = Column(String)
