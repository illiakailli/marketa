#!/usr/bin/env python3

"""Marketa command line interface
Usage:
    marketa (status | watch <symbol> | unwatch <symbol> | pull | purge | monitor | install | uninstall)
    marketa -v|--version
    marketa -h|--help
Commands:
    status              reports it's current configuration status
    watch <symbol>      adds symbol to a watchlist
    unwatch <symbol>    removes symbol from a watchlist
    pull                gets latest historical data for symbols in watchlist filling up local caches
    purge               removes cached data
    monitor             runs marketa in 'monitor' service mode, getting regular updates for watchlist symbols
    install             installs, enables and starts marketa monitor systemd service
    uninstall           uninstalls marketa monitor systemd service
Options:
    -h --help           show this screen
    -v --version        show version
"""

import sys
from time import sleep

from docopt import docopt
from exitstatus import ExitStatus

from marketa import get_facade
from marketa.shared.exceptions import MarketaException
from marketa.shared.utils import hookup_tqdm

facade = get_facade()


def pull():
    print("pulling data for symbols in the watchlist ...")
    facade.pull()
    print("data for symbols in watchlist was succesfully updated")


def watch(symbol: str):
    facade.watch(symbol)
    print(f'symbol "{symbol}" added to the watchlist')


def unwatch(symbol: str):
    facade.unwatch(symbol)
    print(f'symbol "{symbol}" removed from the watchlist')


def monitor():
    try:
        print("marketa monitor mode started", flush=True)
        while True:
            facade.monitor_step()
            sleep(10)

    except KeyboardInterrupt:
        print("\ninterrupted by user")

    except BaseException as e:
        print(f"unexpected error: {e}")
        raise e


def status():
    # import humanize
    watchlist = facade.get_watchlist_symbols()
    result = f"watching {len(watchlist)} symbol(s): "
    result += ", ".join(watchlist)
    """
    result+='\n'
    result+=' data last updated: ' 
    if 'data_updated_on' in s.keys():
        result += humanize.naturaltime(datetime.now() - s['data_updated_on'])
    else:
        result+='never'
    """
    print(result)


def purge():
    facade.purge()
    print("cache cleaned succesfully")


def run():
    try:
        arguments = docopt(str(__doc__))
        hookup_tqdm()  # enable tqdm progress bar

        if arguments["pull"]:
            pull()
        elif arguments["purge"]:
            purge()
        elif arguments["watch"]:
            watch(arguments["<symbol>"][0])
        elif arguments["unwatch"]:
            unwatch(arguments["<symbol>"][0])
        elif arguments["status"]:
            status()
        elif arguments["monitor"]:
            monitor()
        elif arguments["install"]:
            from marketa.install import install

            install()
        elif arguments["uninstall"]:
            from marketa.install import uninstall

            uninstall()
        elif arguments["--version"]:
            # semver major.minor.patch
            # trunk-ignore(mypy/note)
            # trunk-ignore(mypy/import)
            import pkg_resources

            version = pkg_resources.get_distribution("marketa").version
            print(version)
        else:
            print("invalid arguments specified")

    except MarketaException as e:
        print(f"error: {e}")
        sys.exit(ExitStatus.failure)

    except Exception as e:
        print(f"unexpected error: {e}")
        raise e

    sys.exit(ExitStatus.success)


if __name__ == "__main__":
    run()
