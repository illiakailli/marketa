from typing import Final

seconds_in_day: Final[int] = 86400
seconds_in_hour: Final[int] = 3600
systemd_service_path: Final[str] = "/etc/systemd/system/marketa.service"
marketname_nasdaq: Final[str] = "NASDAQ"
