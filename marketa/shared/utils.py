import os
import pathlib
import random
import string
from datetime import datetime
from pathlib import Path
from zoneinfo import ZoneInfo


def check_internet(host="8.8.8.8", port=53, timeout=3) -> bool:
    """
    tests if there is internet connection available
    taken from https://stackoverflow.com/questions/3764291/how-can-i-see-if-theres-an-available-and-active-network-connection-in-python
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    import socket

    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except socket.error:
        return False


def get_cache_folder_path():
    return (
        pathlib.Path(
            os.environ.get("XDG_STATE_HOME")
            or pathlib.Path.home().joinpath(".local", "state")
        )
        .joinpath("marketa", "cache")
        .resolve()
    )


chars = string.ascii_letters + string.octdigits


def random_string_generator(str_size, allowed_chars=chars):
    return "".join(random.choice(allowed_chars) for x in range(str_size))


def hookup_tqdm():
    import sys

    import tqdm

    import marketa.shared.taskeeper as taskeeper

    mod = sys.modules[__name__]
    # hookup tdqm with taskeeper
    def task_start(steps: int):
        mod.pbar = tqdm.tqdm(total=steps)

    taskeeper.on_start_callback = task_start

    def step_start(name: str):
        mod.pbar.set_description(name)

    taskeeper.on_step_start_callback = step_start

    def step_finish():
        mod.pbar.update()

    taskeeper.on_step_finish_callback = step_finish

    def task_finish(info: str):
        mod.pbar.set_description(info)
        mod.pbar.close()

    taskeeper.on_finish_callback = task_finish


def is_virtualenv():
    return "VIRTUAL_ENV" in os.environ


def get_virtualenv_path():
    return os.environ["VIRTUAL_ENV"]


def is_root():
    return os.geteuid() == 0


def file_replace_string(src: Path, dst: Path, old_string: str, new_string: str):
    """creates a new file with all occurrences of old_string replaced by new_string"""
    # safely read the input filename using 'with'
    with open(src) as f:
        s = f.read()
        if old_string not in s:
            raise Exception(
                '"{old_string}" not found in {filename}.'.format(**locals())
            )

    # safely write the changed content, if found in the file
    with open(dst, "w") as f:
        s = s.replace(old_string, new_string)
        f.write(s)


def is_systemd_installed() -> bool:
    result = os.popen(
        'test -x /run/systemd/system && echo "systemd" || echo "other"'
    ).read()
    return "systemd" in result


def is_systemd_service_running(name: str) -> bool:
    status = os.system(f"systemctl is-active --quiet {name}")
    return status == 0


def gmt_now():
    """in database it should be stored as gmt"""
    return datetime.now().astimezone(ZoneInfo(key="GMT"))
