from typing import Callable, TypeVar

import lagom

from ..database.repository import Repository
from ..facade import Facade
from ..feeds.nasdaq import FeedNasdaq

TAbstract = TypeVar("TAbstract")
T = TypeVar("T")


class Container:
    def __init__(self):
        self.container = lagom.Container()
        self.register_singleton(Facade)
        self.register_singleton(FeedNasdaq)
        self.register_singleton(Repository)

    def register_singleton_pair(
        self, tabstract: TAbstract, tconcrete: T, override=False
    ):
        if override and tabstract in self.container._registered_types:
            self.container._registered_types.pop(tabstract, None)
        self.container[tabstract] = lagom.Singleton(tconcrete)

    def register_singleton(self, type):
        self.container[type] = lagom.Singleton(type)

    def register_singleton_factory(self, type: T, factory: Callable[[], T]):
        self.container[type] = factory

    def resolve(self, type: T) -> T:
        result = self.container[type]
        if not result:
            raise Exception(f"unable to instantiate type {type}")
        return result
