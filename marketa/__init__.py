_container = None


def get_facade():
    global _container
    from .facade import Facade
    from .shared.container import Container

    if _container is None:
        _container = Container()

    return _container.resolve(Facade)
