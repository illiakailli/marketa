import shutil

import matplotlib
import pandas as pd
from matplotlib import pyplot as plt
from pandas import DataFrame

from marketa.database.price_daily import PriceDaily
from marketa.database.price_intraday import PriceIntraday
from marketa.feeds.nasdaq import FeedNasdaq
from marketa.shared.constants import marketname_nasdaq

from .database.repository import Repository
from .shared.exceptions import MarketaException
from .shared.taskeeper import taskeeper
from .shared.utils import check_internet as is_online


class Facade:
    def __init__(self, repository: Repository, feed_nasdaq: FeedNasdaq):
        self._repository = repository
        self._feed_nasdaq = feed_nasdaq
        self.__add_extension_methods()  # delay extension methods generation

    def get_watchlist_symbols(self):
        return self._repository.get_watchlist_symbols()

    def is_watching(self, symbol: str):
        symbol = symbol.upper()
        symbols = self.get_watchlist_symbols()
        return symbol in symbols

    def watch(self, symbol: str):
        symbol = symbol.upper()
        if symbol in self.get_watchlist_symbols():
            raise MarketaException(f"symbol {symbol} is already in the watchlist")
        self._repository.add_watchlist_item(symbol)

    def unwatch(self, symbol: str):
        symbol = symbol.upper()
        if symbol not in self.get_watchlist_symbols():
            raise MarketaException(f"symbol {symbol} is not in the watchlist")
        self._repository.remove_watchlist_item(symbol)

    def pull(self):
        if not is_online():
            raise MarketaException(
                "unable to pull data as there is no internet connection available"
            )

        watchlist = self._repository.get_watchlist_symbols()
        market_data = self._feed_nasdaq.load_market_data()
        market = self._repository.upsert_market(market_data)

        with taskeeper(steps=len(watchlist) + 1) as task:
            with task.step(f"updating tradable {market.name} market instruments"):
                tradable_instruments = self._feed_nasdaq.load_instruments()
                tradable_symbols = list(map(lambda x: x.symbol, tradable_instruments))
                self._repository.upsert_instruments(tradable_instruments)

            for sym in watchlist:
                if sym not in tradable_symbols:
                    raise Exception(f'symbol "{sym}" in not listed on nasdaq exchange')

                with task.step(f'updating "{sym}" instrument data'):
                    data = self._feed_nasdaq.load_instrument_data(sym)
                    data.market_id = market.id
                    self._repository.upsert_instrument_data(data)

            task.set_info(f"data successfully loaded for {len(watchlist)} symbol(s)")

    def get_tradable_symbols(self):
        return self._repository.get_unique_symbols()

    def get_prices(self, symbol: str, intraday=True) -> DataFrame:
        market = self._repository.get_market_by_name(marketname_nasdaq)
        prices_daily = self._repository.get_prices_daily(market.id, symbol)

        df = DataFrame()
        for index, price in enumerate(prices_daily):
            if not price.date:
                continue
            # insert empty row so we have place to insert values
            df.append(pd.Series(dtype="object"), ignore_index=True)

            for column in PriceDaily.__table__.columns:
                colname = column.name
                if colname in [
                    "id",
                    "market_id",
                    "instrument_id",
                    "market",
                    "instrument",
                ]:
                    continue
                if colname == "date":
                    colname = "timestamp"
                df.loc[index, colname] = price.__dict__[column.name]

            df.loc[index, "market"] = price.market.name
            df.loc[index, "symbol"] = price.instrument.symbol

        if intraday:
            prices_intraday = self._repository.get_prices_intraday(market.id, symbol)
            for index, price in enumerate(prices_intraday):
                if not price.datetime:
                    continue
                # insert empty row so we have place to insert values
                df.append(pd.Series(dtype="object"), ignore_index=True)

                for column in PriceIntraday.__table__.columns:
                    colname = column.name
                    if colname in [
                        "id",
                        "market_id",
                        "instrument_id",
                        "market",
                        "instrument",
                    ]:
                        continue
                    if colname == "datetime":
                        colname = "timestamp"
                    df.loc[index, colname] = price.__dict__[column.name]

        # set timestamp column as rows index
        df["timestamp"] = pd.to_datetime(df["timestamp"])
        df.set_index("timestamp", inplace=True)

        return df

    def monitor_step(self):
        syms = self.get_watchlist_symbols()
        # mcal
        for sym in syms:
            data = self._feed_nasdaq.load_instrument_data(sym, load_history=False)
            last_price: PriceIntraday = data.prices_intraday[0]
            self._repository.insert_price_intraday()
            print(f"symbol: {data.instrument.symbol} price: {last_price.price}")

    def plot(self, data: DataFrame):
        mpl_backend = "QTAgg"
        matplotlib.use(mpl_backend)
        with plt.ion():
            plt.plot([1, 2, 3])
        raise NotImplementedError()

    def purge(self):
        path = self._repository.get_path()
        shutil.rmtree(path, ignore_errors=True)
        self._repository._ensure_seeded()

    def __add_extension_methods(self):
        from .shared import extensions

        del extensions
