# marketa

marketa is a high-level market analysis library leveraging multiple more verbose libraries behind the scenes

## this project is pre-alpha and likely broken, please ignore it for now

## installation

### from pip

```bash
pip install marketa       # installs library and cli
marketa --version         # verify it works
```

### from pip with monitor service

```bash
sudo pip install marketa  # install as sudo because of the next step
sudo marketa --install    # installs systemd monitor service, so it needs root privileges
marketa --version         # verify it works
```

### from sources

```bash
git clone https://gitlab.com/illiakailli/marketa.git
pip install poetry  # venv, packaging and dependency management tool
poetry install      # gets project dependencies
poetry shell        # activate virtual environment in a separate shell
marketa --version   # verify it works
```

<!---
## Usage

```python
import marketa
mk = marketa.get_facade()
aaau = mk.get_prices('AAAU')
hui = mk.get_prices('HUI')
```

## tbd
- add watchlists
- move cache_folder_path to more standard location, e.g. ~/.cache/marketa
- add indicators
  - linear regression
    ```python
    linreg = mk.create_indicator_linreg(aaau)
    linreg.slope
    ```

  - bollinger bands
    ```python
    bbands = mk.create_indicator_bbands(aaau)
    bbands.top
    ```

  - similarity
    ```python
    sima = mk.create_indicator_sima(aaau, hui)
    sima.trend
    ```

  - causual chains analysis
    ```python
    ins = mk.get_cached_instruments()
    chains = mk.create_indicator_chains(ins)
    hypos = chains.generate_hypotheses()
    hypo = hyps
      .order_by_desc(lambda x: x.probability)
      # take 5 or less most probable hypotheses
      .take_atmost(5)
      .order_by(lambda x: len(x.chain))
      # take the shortest causal chain
      .first()
    hypo.chain.print()
    ```
-->
